package models;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import play.db.jpa.Model;
import utils.LatLng;

@Entity
public class Residence extends Model
{
  @ManyToOne
  public User user;
  public String geolocation; 
  public Date dateRegistered;
  public String rented;
  public int rent;
  public int numberBedrooms;
  public int numberBathrooms;
  public String residenceType;

  public void addUser(User user)
  {
    this.user = user;
    this.save();
  }
  
  public LatLng getGeolocation()
  {
    return LatLng.toLatLng(this.geolocation);
  }
}