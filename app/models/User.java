package models;

import javax.persistence.Entity;
import play.db.jpa.Model;

@Entity
public class User extends Model
{
  public String firstName;
  public String lastName;
  public String email;
  public String password;


  public static User findByEmail(String email)
  {
    return find("byEmail", email).first();
  }

  public boolean checkPassword(String password)
  {
    return this.password.equals(password);
  }

}