package controllers;

import models.User;
import play.Logger;
import play.mvc.Controller;

public class Account extends Controller
{

  public static void index()
  {
    render();
  }


  /**
   * Registers a user onto the MyRent system
   * 
   * @param firstName
   *          the user's first name
   * @param lastName
   *          the user's last name
   * @param email
   *          the user's email address
   * @param password
   *          the user's password, in plain text, naturally :-)
   */
  public static void register(User user)
  {
    user.save();
    login();
  }

  /**
   * Authenticates that a user's login credentials are valid
   * 
   * @param email
   *          The submitted email address
   * @param password
   *          The corresponding password
   */
  /**
   * Authenticates that a user's login credentials are valid
   * 
   * @param email
   *          The submitted email address
   * @param password
   *          The corresponding password
   */
  public static void authenticate(String email, String password)
  {
    User user = User.findByEmail(email);
    if ((user != null) && (user.checkPassword(password) == true))
    {
      Logger.info("Authentication successful");
      session.put("logged_in_userid", user.id);
      session.put("logged_status", "logged_in");
      InputData.index();
    }
    else
    {
      Logger.info("Authentication failed");
      login();
    }
  }

  public static void login()
  {
    render();
  }

  public static void logout()
  {
    setSessionLogout();
    Home.index();
  }

  /*
   * clear session on logout
   */
  protected static void setSessionLogout()
  {
    if (session.get("logged_status") != null && session.get("logged_status").equals("logged_in"))
    {
      session.clear();
      session.put("logged_status", "logged_out");
    }
  }
  
  public static User getCurrentUser()
  {
    User user = null;
    if (session.contains("logged_in_userid"))
    {
      String userId = session.get("logged_in_userid");
      user = User.findById(Long.parseLong(userId));
    }
    return user;
  }
}